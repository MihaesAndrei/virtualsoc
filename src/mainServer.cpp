#include "Server/Server.h"

int main() {
    try {
        Server *server = new Server(8321);
        server->start();
    } catch (char const *string) {
        perror(string);
    }
}