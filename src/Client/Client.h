#ifndef VIRTUALSOC_CLIENT_H
#define VIRTUALSOC_CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>

class Client {

private:
    int* socketDescriptor;			// descriptorul de socket
    struct sockaddr_in server;	// structura folosita pentru conectare
    uint16_t port;

public:
    Client(char* ip, char * port);
    int* getSocketDescriptor();

private:
    int createSocket(int domain, int type, int protocol);
    void initializeServer(char* ip, char* port);
    void connectToServer();
};
#endif //VIRTUALSOC_CLIENT_H
