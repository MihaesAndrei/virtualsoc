#ifndef VIRTUALSOC_CONNECTFRAME_H
#define VIRTUALSOC_CONNECTFRAME_H


#include <QtCore>
#include <QFrame>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>

#include "../Client.h"

class ConnectMenu : public QPushButton{

Q_OBJECT

public:
    ConnectMenu(QWidget *parent = 0);
    ~ConnectMenu();

    QFrame* getFrame();

    char* getIP();
    char* getPort();

private:
    QFrame *frame;
    QGridLayout *grid;

    QLabel *ipServer;
    QLabel *portServer;
    QLineEdit *ip;
    QLineEdit *port;
};

#endif //VIRTUALSOC_CONNECTFRAME_H
