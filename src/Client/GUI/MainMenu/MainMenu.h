#ifndef VIRTUALSOC_MAINMENU_H
#define VIRTUALSOC_MAINMENU_H

#include <QWidget>
#include <QFrame>
#include <QPushButton>
#include <QBoxLayout>
#include <src/Client/GUI/MainMenu/Chat/ChatRoomFrame.h>

#include "AddFriendFrame.h"
#include "ViewFriendListFrame.h"
#include "PrivacySettingsFrame.h"
#include "MakePostFrame.h"
#include "RecentPostsFrame.h"

class MainMenu : public QWidget {

Q_OBJECT

public:
    MainMenu(QWidget *parent = 0);

    QFrame* getFrame();
    void setVisibility(bool visible);

    void removeMainMenu();
    void addMainMenu();

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

    AddFriendFrame * addFriendFrame;
    ViewFriendListFrame * viewFriendListFrame;
    PrivacySettingsFrame * privacySettingsFrame;
    MakePostFrame *makePostFrame;
    RecentPostsFrame *recentPostsFrame;
    ChatRoomFrame *chatRoomFrame;

    QPushButton *logOut;

private slots:
    void addFriendListener();
    void backFromAddFriend();

    void viewFriendListListener();
    void backFromViewFriendList();

    void privacySettingsListener();
    void backFromPrivacySettings();

    void makePostListener();
    void backFromMakePost();

    void viewRecentPostsListener();
    void backFromRecentPosts();

    void chatRoomListener();
    void backFromChatRoom();

private:
    QFrame *frame;
    QGridLayout *grid;

    int *socketDescriptor;

    QPushButton *addFriendButton;
    QPushButton *viewFriendListButton;
    QPushButton *privacySettingsButton;
    QPushButton *makePostButton;
    QPushButton *viewRecentPostsButton;
    QPushButton *chatRoom;

    QPushButton *backFromAddFriendButton;
    QPushButton *backFromViewFriendListButton;
    QPushButton *backFromPrivacySettingsButton;
    QPushButton *backFromMakePostButton;
    QPushButton *backFromRecentPostsButton;
    QPushButton *backFromChatRoomButton;

    QLabel *welcomeMessage;

public:
    void setWelcomeMessage(char * email);
};

#endif //VIRTUALSOC_MAINMENU_H
