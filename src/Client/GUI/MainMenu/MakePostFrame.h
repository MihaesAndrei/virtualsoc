#ifndef VIRTUALSOC_MAKEPOSTFRAME_H
#define VIRTUALSOC_MAKEPOSTFRAME_H

#include <QWidget>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QTextEdit>
#include <QPushButton>

#include "src/Client/Options.h"

class MakePostFrame : public QWidget {

Q_OBJECT

public:
    MakePostFrame(QWidget *parent = 0);
    QFrame* getFrame();

    void setVisibility(bool visibile);

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

private slots:
    void checkPublic(int state);
    void checkFamily(int state);
    void checkPrivate(int state);
    void postListener();

private:
    QFrame *frame;
    QGridLayout *grid;

    QLabel *privacyLabel;
    QCheckBox *publicPrivacy;
    QCheckBox *familyPrivacy;
    QCheckBox *privatePrivacy;
    char* privacy;

    QLabel *postLabel;
    QTextEdit *postText;

    QPushButton *postButton;

    int *socketDescriptor;
};


#endif //VIRTUALSOC_MAKEPOSTFRAME_H
