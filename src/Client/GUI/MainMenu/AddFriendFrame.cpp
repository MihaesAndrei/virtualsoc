#include "AddFriendFrame.h"

AddFriendFrame::AddFriendFrame(QWidget *parent) {

    frame = new QFrame(this);

    emailLabel = new QLabel("E-mail: ", this);
    typeLabel = new QLabel("Type: ", this);
    emailFriend = new QLineEdit("", this);

    friendType = new QCheckBox("Friend", this);
    connect(friendType, SIGNAL(stateChanged(int)), this, SLOT(checkFriend(int)));
    familyType = new QCheckBox("Family", this);
    connect(familyType, SIGNAL(stateChanged(int)), this, SLOT(checkFamily(int)));

    addFriendButton = new QPushButton("Add Friend", this);
    connect(addFriendButton, SIGNAL(clicked()), this, SLOT(addFriendListener()));

    grid = new QGridLayout(this);
    grid->addWidget(emailLabel, 0, 0);
    grid->addWidget(emailFriend, 0, 1);
    grid->addWidget(typeLabel, 1, 0);
    grid->addWidget(friendType, 1, 1);
    grid->addWidget(familyType, 2, 1);
    grid->addWidget(addFriendButton, 3, 1);

    frame->setLayout(grid);
}

QFrame *AddFriendFrame::getFrame() {
    return frame;
}

void AddFriendFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int AddFriendFrame::getSocketDescriptor() {
    return *socketDescriptor;
}

void AddFriendFrame::addFriendListener() {
    if(strcmp(emailFriend->text().toLatin1().data(), "") == 0)
        QMessageBox::warning(NULL, "Message", "You didn't insert email!\n");
    else if(friendType->checkState() == Qt::Unchecked && familyType->checkState() == Qt::Unchecked)
        QMessageBox::warning(NULL, "Message", "You need to choose a type of relationship.\n");
    else {
        int result = Options::addFriend(emailFriend->text().toLatin1().data(),
                                        type,
                                        getSocketDescriptor());
        if (result == -1) {
            QMessageBox::warning(NULL, "Message", "Invalid type!\nAvailable types: Friend, Family.\n");
        }
        if (result == 0) {
            char *buf = emailFriend->text().toAscii().data();
            buf = Utility::concatenate(buf, (char *) " is already in your friend list.\nYou can't add him anymore.");
            QMessageBox::warning(NULL, "Message", buf);
            emailFriend->setText("");
        }
        if (result == 1) {
            char *buf = emailFriend->text().toAscii().data();
            buf = Utility::concatenate(buf, (char *) " has been added successfully to your friend list.");
            QMessageBox::information(NULL, "Message", buf);
            friendType->setCheckState(Qt::Unchecked);
            familyType->setCheckState(Qt::Unchecked);
            emailFriend->setText("");
        }
    }
}

void AddFriendFrame::checkFriend(int state) {

    if(state == Qt::Checked) { familyType->setCheckState(Qt::Unchecked); type = (char *) "Friend"; }
}

void AddFriendFrame::checkFamily(int state) {
    if(state == Qt::Checked) { friendType->setCheckState(Qt::Unchecked); type = (char *) "Family"; }
}
