#ifndef VIRTUALSOC_RECENTPOSTSFRAME_H
#define VIRTUALSOC_RECENTPOSTSFRAME_H


#include <qwidget.h>
#include <qframe.h>
#include <qgridlayout.h>
#include <qscrollarea.h>
#include <qlabel.h>

#include "src/Client/Options.h"

class RecentPostsFrame : public QWidget {

Q_OBJECT

public:
    RecentPostsFrame(QWidget *parent  = 0);
    QFrame* getFrame();

    void setVisibility(bool visible);

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

private:
    QFrame *frame;
    QGridLayout *grid;

    int *socketDescriptor;

    QScrollArea *scrollArea;
    QLabel *recentPosts;


};


#endif //VIRTUALSOC_RECENTPOSTSFRAME_H
