//
// Created by andrei on 12.01.2016.
//

#include <src/Client/Options.h>
#include "AdminMainMenu.h"

AdminMainMenu::AdminMainMenu(QWidget *parent, int *socketDescriptor) {
    this->socketDescriptor = socketDescriptor;

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame *AdminMainMenu::getFrame() {
    vector<UserInfo> usersInfo = Options::getUsersList(*socketDescriptor);

    QFont font( "Arial", 15);
    QLabel *title = new QLabel("Users", frame);
    title->setTextFormat(Qt::TextFormat::RichText);
    title->setFont(font);

    table = new QTableWidget((int) usersInfo.size(), 5);
    QTableWidgetItem *firstname = new QTableWidgetItem("First name");
    QTableWidgetItem *surname = new QTableWidgetItem("Surname");
    QTableWidgetItem *email = new QTableWidgetItem("E-mail");
    QTableWidgetItem *status = new QTableWidgetItem("Status");
    QTableWidgetItem *ip = new QTableWidgetItem("IP");
    table->setHorizontalHeaderItem(0, firstname);
    table->setHorizontalHeaderItem(1, surname);
    table->setHorizontalHeaderItem(2, email);
    table->setHorizontalHeaderItem(3, status);
    table->setHorizontalHeaderItem(4, ip);

    for (int i = 0; i < usersInfo.size(); i++) {
        QTableWidgetItem *firstnameItem = new QTableWidgetItem(usersInfo[i].firstname);
        QTableWidgetItem *surnameItem = new QTableWidgetItem(usersInfo[i].surname);
        QTableWidgetItem *emailItem = new QTableWidgetItem(usersInfo[i].email);
        QTableWidgetItem *statusItem = new QTableWidgetItem(usersInfo[i].status);
        QTableWidgetItem *ipItem = new QTableWidgetItem(usersInfo[i].ip);
        table->setItem(i, 0, firstnameItem);
        table->setItem(i, 1, surnameItem);
        table->setItem(i, 2, emailItem);
        table->setItem(i, 3, statusItem);
        table->setItem(i, 4, ipItem);
    }

    grid->addWidget(title, 0, 0, Qt::AlignCenter);
    grid->addWidget(table, 1, 0, Qt::AlignCenter);
    table->setMinimumWidth(400);
    return frame;
}

void AdminMainMenu::setVisibility(bool visible) {
    frame->setVisible(visible);
}
