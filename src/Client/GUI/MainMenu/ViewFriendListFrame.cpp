#include "ViewFriendListFrame.h"

ViewFriendListFrame::ViewFriendListFrame(QWidget *parent) {

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame *ViewFriendListFrame::getFrame() {

    vector<MyFriend> myFriends= Options::viewFriendList(*socketDescriptor);

    QFont font( "Arial", 12);
    QLabel *title = new QLabel("Friend list:", frame);
    title->setTextFormat(Qt::TextFormat::RichText);
    title->setFont(font);

    table = new QTableWidget((int) myFriends.size(), 3);
    QTableWidgetItem *email = new QTableWidgetItem("E-mail");
    QTableWidgetItem *type = new QTableWidgetItem("Type");
    QTableWidgetItem *status = new QTableWidgetItem("Status");
    table->setHorizontalHeaderItem(0, email);
    table->setHorizontalHeaderItem(1, type);
    table->setHorizontalHeaderItem(2, status);

    for (int i = 0; i < myFriends.size(); i++) {
        QTableWidgetItem *emailItem = new QTableWidgetItem(myFriends[i].email);
        QTableWidgetItem *typeItem = new QTableWidgetItem(myFriends[i].type);
        QTableWidgetItem *statusItem = new QTableWidgetItem(myFriends[i].status);
        table->setItem(i, 0, emailItem);
        table->setItem(i, 1, typeItem);
        table->setItem(i, 2, statusItem);
    }

    grid->addWidget(title, 0, 0, Qt::AlignCenter);
    grid->addWidget(table, 1, 0, Qt::AlignCenter);
    table->setMinimumWidth(318);

    return this->frame;
}

void ViewFriendListFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int ViewFriendListFrame::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void ViewFriendListFrame::setVisibility(bool visible) {
    frame->setVisible(visible);
}
