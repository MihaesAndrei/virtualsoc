#include <qscrollarea.h>
#include "PrivacySettingsFrame.h"

PrivacySettingsFrame::PrivacySettingsFrame(QWidget *parent) {
    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame *PrivacySettingsFrame::getFrame() {
    settings = Options::getPrivacySettings(*socketDescriptor);

    profile = new QLabel("Profile Privacy", this);
    publicProfile = new QCheckBox("Public", this);
    privateProfile = new QCheckBox("Private", this);

    posts = new QLabel("Posts Privacy:", this);
    publicPosts = new QCheckBox("Public", this);
    privatePosts = new QCheckBox("Private", this);

    connect(publicProfile, SIGNAL(stateChanged(int)), this, SLOT(checkPublicProfile(int)));
    connect(privateProfile, SIGNAL(stateChanged(int)), this, SLOT(checkPrivateProfile(int)));
    connect(publicPosts, SIGNAL(stateChanged(int)), this, SLOT(checkPublicPosts(int)));
    connect(privatePosts, SIGNAL(stateChanged(int)), this, SLOT(checkPrivatePosts(int)));

    if (strcmp(settings->profilePrivacy, "Public") == 0 ) publicProfile->setCheckState(Qt::Checked);
    else privateProfile->setCheckState(Qt::Checked);


    if (strcmp(settings->postsPrivacy, "Public") == 0) publicPosts->setCheckState(Qt::Checked);
    else privatePosts->setCheckState(Qt::Checked);

    grid->addWidget(profile, 0, 0);
    grid->addWidget(publicProfile, 0, 1);
    grid->addWidget(privateProfile, 1, 1);
    grid->addWidget(posts, 2, 0);
    grid->addWidget(publicPosts, 2, 1);
    grid->addWidget(privatePosts, 3, 1);
    grid->setSpacing(1);
    grid->setAlignment(Qt::AlignmentFlag::AlignTop);
    return this->frame;
}


void PrivacySettingsFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int PrivacySettingsFrame::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void PrivacySettingsFrame::setVisibility(bool visible) {
    frame->setVisible(visible);
}

void PrivacySettingsFrame::checkPublicProfile(int state) {
    if(state == Qt::Checked) {
        privateProfile->setCheckState(Qt::Unchecked);
        Options::setProfilePrivacy((char *) "Public", *socketDescriptor);
    }
}

void PrivacySettingsFrame::checkPrivateProfile(int state) {
    if(state == Qt::Checked) {
        publicProfile->setCheckState(Qt::Unchecked);
        Options::setProfilePrivacy((char *) "Private", *socketDescriptor);
    }
}

void PrivacySettingsFrame::checkPublicPosts(int state) {
    if(state == Qt::Checked) {
        privatePosts->setCheckState(Qt::Unchecked);
        Options::setPostsPrivacy((char *) "Public", *socketDescriptor);
    }
}

void PrivacySettingsFrame::checkPrivatePosts(int state) {
    if(state == Qt::Checked) {
        publicPosts->setCheckState(Qt::Unchecked);
        Options::setPostsPrivacy((char *) "Private", *socketDescriptor);
    }
}
