#ifndef VIRTUALSOC_CONVERSATIONFRAMEPARTICIPANT_H
#define VIRTUALSOC_CONVERSATIONFRAMEPARTICIPANT_H


#include <qwidget.h>
#include <qframe.h>
#include <qgridlayout.h>
#include <qlineedit.h>
#include <qlistwidget.h>
#include <qtextedit.h>
#include <qpushbutton.h>
#include "MessagesFrame.h"

class ConversationFrame : public QWidget {

Q_OBJECT

public:
    ConversationFrame(QWidget *parent  = 0, int *fd = 0);
    QFrame* getFrame();

    void setVisibility(bool visible);
    void setConversationName(char* c);

    MessagesFrame * messagesFrame;

private slots:
    void sendListener();

private:
    QFrame *frame;
    QGridLayout *grid;

    int *socketDescriptor;

    QListWidget *listWidget;

    QLabel *text;
    QTextEdit *message;
    QPushButton *send;

    QLabel *conversationNameLabel;
    char *conversationName;

    void addParticipants() const;
};


#endif //VIRTUALSOC_CONVERSATIONFRAMEPARTICIPANT_H
