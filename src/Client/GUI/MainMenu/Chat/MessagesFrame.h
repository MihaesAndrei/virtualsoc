#ifndef VIRTUALSOC_MESSAGESFRAME_H
#define VIRTUALSOC_MESSAGESFRAME_H


#include <qwidget.h>
#include <qscrollarea.h>
#include <qlabel.h>
#include <qgridlayout.h>
#include <src/Client/Options.h>

class MessagesFrame : public QScrollArea {

Q_OBJECT

public:
    MessagesFrame(QWidget *parent = 0, int *fd = 0);

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

    static QWidget *parent;

private:

    static int *socketDescriptor;

public:
    QScrollArea *scrollArea;
    static QLabel *text;
    static char* conversationName;
    static void *update(void *);

};


#endif //VIRTUALSOC_MESSAGESFRAME_H
