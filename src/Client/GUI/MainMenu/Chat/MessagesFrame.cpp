#include <qscrollbar.h>
#include "MessagesFrame.h"

QWidget* MessagesFrame::parent;
QLabel* MessagesFrame::text;
char* MessagesFrame::conversationName;
int * MessagesFrame::socketDescriptor;
MessagesFrame::MessagesFrame(QWidget *parent, int *fd) : QScrollArea(parent) {

    this->parent = parent;

    socketDescriptor = fd;

    this->setMinimumWidth(300);
    this->setMaximumWidth(300);

    text = new QLabel(this);

    QFont font("Arial", 12);
    text->setFont(font);

    this->setBackgroundRole(QPalette::Light);
    this->setWidget(text);

    pthread_t thread;
    if (pthread_create(&thread, NULL, update, NULL) != 0)
        throw "[server] Error: failed to create thread.\n";
    pthread_detach(pthread_self());
}

void* MessagesFrame::update(void* ) {
    QString string;

    string = Options::getMessages(conversationName, *socketDescriptor);
    text->setText(string);
    text->adjustSize();
}

void MessagesFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int MessagesFrame::getSocketDescriptor() {
    return *socketDescriptor;
}
