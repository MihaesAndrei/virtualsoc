#include "ChatRoomFrame.h"

ChatRoomFrame::ChatRoomFrame(QWidget *parent) : QWidget(parent) {

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame *ChatRoomFrame::getFrame() {
    openConversationButton = new QPushButton("Open Conversation", this);
    connect(openConversationButton, SIGNAL(clicked()), this, SLOT(openConversationListener()));

    createConversationButton = new QPushButton("Create Conversation", this);
    connect(createConversationButton, SIGNAL(clicked()), this, SLOT(createConversationListener()));

    friendListLabel = new QLabel("Friend list", this);

    listWidget = new QListWidget(this);
    listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

    conversationsWidget = new QListWidget(this);

    addFriendsToListWidget();
    addConversationsToListWidget();

    conversationsLabel = new QLabel("Conversations", this);

    createConversation = new QLabel("Conversation name: ", this);
    name = new QLineEdit("", this);


    grid->addWidget(friendListLabel, 0, 1 , Qt::AlignmentFlag::AlignTop);
    grid->addWidget(listWidget, 1, 1, Qt::AlignmentFlag::AlignRight);
    grid->addWidget(conversationsLabel, 0, 2, Qt::AlignmentFlag::AlignLeft);
    grid->addWidget(conversationsWidget, 1, 2, Qt::AlignmentFlag::AlignRight);
    grid->addWidget(openConversationButton, 2, 2, Qt::AlignmentFlag::AlignCenter);
    grid->addWidget(createConversationButton, 2, 1, Qt::AlignmentFlag::AlignCenter);
    grid->addWidget(name, 3, 1, Qt::AlignmentFlag::AlignCenter);

    return frame;
}

void ChatRoomFrame::addConversationsToListWidget() {
    conversationsList = Options::getConversationsList(*socketDescriptor);
    for (int i = 0; i < conversationsList.size(); i++) {
        new QListWidgetItem(conversationsList.at(i).data(), conversationsWidget);
    }
}

void ChatRoomFrame::addFriendsToListWidget() {
    myFriends = Options::viewFriendList(*socketDescriptor);
    for (int i = 0; i < myFriends.size(); i++) {
        char *item = (char *) "";
        item = Utility::concatenate(item, myFriends.at(i).email);
        item = Utility::concatenate(item, (char *) " ---  ");
        item = Utility::concatenate(item, myFriends.at(i).status);
        new QListWidgetItem(item, listWidget);
    }
}

void ChatRoomFrame::setVisibility(bool visible) {
    openConversationButton->setVisible(visible);
    createConversationButton->setVisible(visible);
    listWidget->setVisible(visible);
    friendListLabel->setVisible(visible);
    createConversation->setVisible(visible);
    name->setVisible(visible);
    conversationsWidget->setVisible(visible);
    conversationsLabel->setVisible(visible);
}

void ChatRoomFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int ChatRoomFrame::getSocketDescriptor() {
    return *socketDescriptor;
}

void ChatRoomFrame::removeMainMenu() {
    grid->removeWidget(createConversationButton);
    grid->removeWidget(openConversationButton);
    grid->removeWidget(listWidget);
    grid->removeWidget(friendListLabel);
    grid->removeWidget(conversationsWidget);
    setVisibility(false);
    backFromChatRoomButton->setVisible(false);
}

void ChatRoomFrame::addMainMenu() {

    openConversationButton = new QPushButton("Open Conversation", this);
    connect(openConversationButton, SIGNAL(clicked()), this, SLOT(openConversationListener()));

    createConversationButton = new QPushButton("Create Conversation", this);
    connect(createConversationButton, SIGNAL(clicked()), this, SLOT(createConversationListener()));
    QHBoxLayout *hbox = new QHBoxLayout();
    createConversation = new QLabel("Conversation name: ", this);
    name = new QLineEdit("", this);
    grid->addWidget(friendListLabel, 0, 1 , Qt::AlignmentFlag::AlignTop);
    grid->addWidget(listWidget, 1, 1, Qt::AlignmentFlag::AlignRight);
    grid->addWidget(conversationsLabel, 0, 2, Qt::AlignmentFlag::AlignLeft);
    grid->addWidget(conversationsWidget, 1, 2, Qt::AlignmentFlag::AlignRight);
    grid->addWidget(openConversationButton, 2, 2, Qt::AlignmentFlag::AlignCenter);
    grid->addWidget(createConversationButton, 2, 1, Qt::AlignmentFlag::AlignCenter);
    grid->addWidget(name, 3, 1, Qt::AlignmentFlag::AlignCenter);

    setVisibility(true);
    backFromChatRoomButton->setVisible(true);
}

void ChatRoomFrame::createConversationListener() {

    vector<MyFriend> selectedFriends = getSelectedFriends();
//    debugSelectedFriends(selectedFriends);

    if(selectedFriends.size() == 0) {
        QMessageBox::warning(NULL, "Message", "You didn't select any friends to start conversation with.");
    }
    else if (strcmp(name->text().toLatin1().data(), "") == 0) {
        QMessageBox::warning(NULL, "Message", "You need a conversation name.");
    }
    else if(false == Options::createConversation(name->text().toLatin1().data(), *socketDescriptor)) {
        QMessageBox::warning(NULL, "Message", "Conversation name is already in use.");
    }
    else {
        QMessageBox::information(NULL, "Message", "Conversation created.");
        char* conversationName = name->text().toLatin1().data();
        Options::addOwnerInParticipants(conversationName, *socketDescriptor);
        for (unsigned long i = 0; i < selectedFriends.size(); i++) {
            Options::addParticipant(conversationName, selectedFriends.at(i).email, *socketDescriptor);
        }
        addConversationsToListWidget();
    }
}

void ChatRoomFrame::debugSelectedFriends(vector<MyFriend> &selectedFriends) const {
    for (int i = 0; i < selectedFriends.size(); i++) {
        printf("%s ", selectedFriends.at(i).email);
    }
    printf("\n");
}

vector<MyFriend> ChatRoomFrame::getSelectedFriends() {

    vector<MyFriend> selectedFriends;

    QItemSelectionModel * selections = listWidget->selectionModel();
    QModelIndexList indexes = selections->selectedIndexes();

    for (int i = 0; i < indexes.size(); i++) {
        int index = indexes.at(i).row();
        selectedFriends.push_back(myFriends.at(index));
    }
    return selectedFriends;
}

void ChatRoomFrame::openConversationListener() {

    int index = conversationsWidget->currentRow();

    if(index < 0 ) QMessageBox::warning(NULL, "Message", "You didn't select any conversation from list.");
    else {
        char *conversationName = strcpy((char *) malloc(conversationsList.at(index).length() + 1),
                                        conversationsList.at(index).c_str());

        removeMainMenu();

        backFromOpenConversationButton = new QPushButton("Back", this);
        connect(backFromOpenConversationButton, SIGNAL(clicked()), this, SLOT(backFromOpenConversation()));

        conversationFrameUser = new ConversationFrame(this, socketDescriptor);
        conversationFrameUser->setConversationName(conversationName);

        grid->addWidget(backFromOpenConversationButton, 0, 0, Qt::AlignTop);
        grid->addWidget(conversationFrameUser->getFrame(), 1, 0);
    }
}

void ChatRoomFrame::backFromOpenConversation() {
    delete backFromOpenConversationButton;
    conversationFrameUser->setVisibility(false);
    addMainMenu();
}
