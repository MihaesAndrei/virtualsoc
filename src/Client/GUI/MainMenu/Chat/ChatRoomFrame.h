#ifndef VIRTUALSOC_CHATROOMFRAME_H
#define VIRTUALSOC_CHATROOMFRAME_H


#include <qwidget.h>
#include <qframe.h>
#include <qgridlayout.h>
#include <qpushbutton.h>
#include <qlistwidget.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <vector>
#include <qlineedit.h>
using namespace std;

#include <src/Client/Options.h>
#include "ConversationFrame.h"

class ChatRoomFrame : public QWidget {

Q_OBJECT

public:
    ChatRoomFrame(QWidget *parent = 0);
    QFrame* getFrame();

    void setVisibility(bool visible);

    void removeMainMenu();
    void addMainMenu();

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

    QPushButton *backFromChatRoomButton;
    ConversationFrame *conversationFrameUser;

private:
    void addFriendsToListWidget();
    vector<MyFriend> getSelectedFriends();
    void debugSelectedFriends(vector<MyFriend> &selectedFriends) const;

private slots:
    void createConversationListener();

    void openConversationListener();
    void backFromOpenConversation();

private:
    QFrame *frame;
    QGridLayout *grid;

    QLabel *friendListLabel;
    QListWidget *listWidget;
    vector<MyFriend> myFriends;

    QLabel *conversationsLabel;
    QListWidget *conversationsWidget;
    vector<string> conversationsList;

    int *socketDescriptor;

    QPushButton *createConversationButton;

    QPushButton *openConversationButton;
    QPushButton *backFromOpenConversationButton;
    QLabel *createConversation;
    QLineEdit *name;


    void addConversationsToListWidget();
};


#endif //VIRTUALSOC_CHATROOMFRAME_H
