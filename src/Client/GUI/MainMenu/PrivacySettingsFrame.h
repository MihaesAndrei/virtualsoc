#ifndef VIRTUALSOC_PRIVACYSETTINGSFRAME_H
#define VIRTUALSOC_PRIVACYSETTINGSFRAME_H


#include <qwidget.h>
#include <qgridlayout.h>
#include <qframe.h>
#include <qlabel.h>
#include <qcheckbox.h>

#include "src/Client/Options.h"

class PrivacySettingsFrame : public QWidget{

Q_OBJECT

public:
    PrivacySettingsFrame(QWidget *parent = 0);

    QFrame* getFrame();
    void setVisibility(bool visible);

    int getSocketDescriptor();
    void setSocketDescriptor(int *fd);

private slots:
    void checkPublicProfile(int state);
    void checkPrivateProfile(int state);
    void checkPublicPosts(int state);
    void checkPrivatePosts(int state);

private:
    QFrame *frame;
    QGridLayout *grid;

    PrivacySettings * settings;
    int *socketDescriptor;

    QLabel *profile;
    QCheckBox *publicProfile;
    QCheckBox *privateProfile;
    QLabel *posts;
    QCheckBox *publicPosts;
    QCheckBox *privatePosts;
};


#endif //VIRTUALSOC_PRIVACYSETTINGSFRAME_H
