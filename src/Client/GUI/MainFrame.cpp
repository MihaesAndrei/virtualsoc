#include "MainFrame.h"

MainFrame::MainFrame(QWidget *parent) {

    frame = new QFrame(this);

    btn = new QPushButton("Connect", this);
    connect(btn, SIGNAL(clicked()), this, SLOT(connectListener()));

    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(quitMyApp()));

    grid = new QGridLayout(this);
    connectFrame = new ConnectMenu();
    grid->addWidget(connectFrame->getFrame(), 0, 0);
    grid->addWidget(btn, 1, 0, Qt::AlignCenter);
}

void MainFrame::draw() {
    QDesktopWidget *desktop = QApplication::desktop();
    int screenWidth = desktop->width();
    int screenHeight = desktop->height();
    int WIDTH = 510; int HEIGHT = 400;
    int x = (screenWidth - WIDTH) / 2;
    int y = (screenHeight - HEIGHT ) / 2;

    this->resize(WIDTH, HEIGHT);
    this->move( x, y );
    this->show();
}

void MainFrame::connectListener() {
    try {
        Client *client = new Client(connectFrame->getIP(), connectFrame->getPort());
        setSocketDescriptor(client->getSocketDescriptor());

        connectFrame->getFrame()->setVisible(false);
        btn->setVisible(false);

        connected = true;

        guestMenu = new GuestMenu();
        guestMenu->setSocketDescriptor(socketDescriptor);

        grid->addWidget(guestMenu->getFrame(), 0 , 0);

    }
    catch(const char* string) {
        perror(string);
        QMessageBox::warning(NULL, "Error!", "Conection refused!");
    }
}

void MainFrame::quitMyApp() {
    if (connected) {
        if (*socketDescriptor != 0) {
            if (write(getSocketDescriptor(), "quit", 100) < 0) throw "[client]Error: write() to server.\n";
            _UNISTD_H;
            ::close(getSocketDescriptor());
        }
        QCoreApplication::quit();
    } else QCoreApplication::quit();
}

int MainFrame::getSocketDescriptor() {
    return * this->socketDescriptor;
}

void MainFrame::setSocketDescriptor(int *fd) {
    this->socketDescriptor = fd;
}
