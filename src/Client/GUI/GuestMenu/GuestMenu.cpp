#include "GuestMenu.h"

GuestMenu::GuestMenu(QWidget *parent) {

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame*GuestMenu::getFrame() {
    addGuestMenu();
    return this->frame;
}

void GuestMenu::logInListener() {
    backFromLogIn = new QPushButton("Back", frame);
    connect(backFromLogIn, SIGNAL(clicked()), this, SLOT(backFromLogInListener()));

    removeGuestMenu();

    loginFrame = new LoginFrame();
    loginFrame->setSocketDescriptor(this->socketDescriptor);
    loginFrame->backFromLogIn = backFromLogIn;
    loginFrame->guestMenu = this;

    grid->addWidget(backFromLogIn, 0, 0);
    grid->addWidget(loginFrame->getFrame(), 1, 0);

}

void GuestMenu::setSocketDescriptor(int* fd) {
    this->socketDescriptor = fd;
}

void GuestMenu::quitListener() {
    if (write(getSocketDescriptor(), "quit", 100) < 0) throw "[client]Error: write() to server.\n";
    _UNISTD_H;
    ::close(getSocketDescriptor());
    closeSocket();
    QCoreApplication::quit();
}

int GuestMenu::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void GuestMenu::closeSocket() {
    *this->socketDescriptor = 0;
}

void GuestMenu::backFromLogInListener() {
    backFromLogIn->setVisible(false);
    loginFrame->getFrame()->setVisible(false);
    addGuestMenu();
}

void GuestMenu::signUpListener()  {
    backFromSignUp = new QPushButton("Back", frame);
    connect(backFromSignUp, SIGNAL(clicked()), this, SLOT(backFromSignUpListener()));

    setVisibility(false);

    signUpMenu = new SignUpFrame();
    signUpMenu->setSocketDescriptor(this->socketDescriptor);

    grid->addWidget(backFromSignUp, 0, 0);
    grid->addWidget(signUpMenu->getFrame(), 1, 0);
}

void GuestMenu::backFromSignUpListener() {
    backFromSignUp->setVisible(false);
    grid->removeWidget(signUpMenu);
    signUpMenu->getFrame()->setVisible(false);
    setVisibility(true);
}

void GuestMenu::viewRecentPostsListener() {
    backFromRecentPosts = new QPushButton("Back", frame);
    connect(backFromRecentPosts, SIGNAL(clicked()), this, SLOT(backFromViewRecentPosts()));

    this->setVisibility(false);

    publicPostsFrame = new PublicPostsFrame();
    publicPostsFrame->setSocketDescriptor(this->socketDescriptor);

    grid->addWidget(backFromRecentPosts, 0, 0);
    grid->addWidget(publicPostsFrame->getFrame(), 1, 0);
}

void GuestMenu::backFromViewRecentPosts() {
    backFromRecentPosts->setVisible(false);
    publicPostsFrame->setVisibility(false);
    setVisibility(true);
}

void GuestMenu::setVisibility(bool visible) {
    signUp->setVisible(visible);
    logIn->setVisible(visible);
    viewPublicPosts->setVisible(visible);
    quit->setVisible(visible);
}

void GuestMenu::removeGuestMenu() {
    grid->removeWidget(signUp);
    grid->removeWidget(logIn);
    grid->removeWidget(viewPublicPosts);
    grid->removeWidget(quit);
    setVisibility(false);
}

void GuestMenu::addGuestMenu() {

    signUp = new QPushButton("Sign Up", this);
    connect(signUp, SIGNAL(clicked()), this, SLOT(signUpListener()));

    logIn = new QPushButton("Login", this);
    connect(logIn, SIGNAL(clicked()), this, SLOT(logInListener()));

    viewPublicPosts = new QPushButton("View Public Posts", this);
    connect(viewPublicPosts, SIGNAL(clicked()), this, SLOT(viewRecentPostsListener()));

    quit = new QPushButton("Quit", this);
    connect(quit, SIGNAL(clicked()), this, SLOT(quitListener()));

    grid->addWidget(signUp, 0, 0, Qt::AlignCenter);
    grid->addWidget(logIn, 1, 0, Qt::AlignCenter);
    grid->addWidget(viewPublicPosts, 2, 0, Qt::AlignCenter);
    grid->addWidget(quit, 3, 0, Qt::AlignCenter);
    signUp->setMinimumWidth(110);
    logIn->setMinimumWidth(110);
    viewPublicPosts->setMinimumWidth(110);
    quit->setMinimumWidth(110);

    setVisibility(true);
}
