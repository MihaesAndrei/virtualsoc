#include "LoginFrame.h"

LoginFrame::LoginFrame(QWidget *parent) {

    frame = new QFrame(this);

    logInButton = new QPushButton("Log In", this);
    connect(logInButton, SIGNAL(clicked()), this, SLOT(logInButtonListener()));

    emailLabel = new QLabel("E-mail: ", this);
    passwordLabel = new QLabel("Password: ", this);

    email = new QLineEdit("", this);
    password = new QLineEdit("", this);
    password->setEchoMode(QLineEdit::Password);

    grid = new QGridLayout(this);
    grid->addWidget(emailLabel, 0, 0);
    grid->addWidget(email, 0, 1);
    grid->addWidget(passwordLabel, 1, 0);
    grid->addWidget(password, 1, 1);
    grid->addWidget(logInButton, 2, 1);

    asAdministrator = new QCheckBox("As Administrator", this);
    connect(asAdministrator, SIGNAL(stateChanged(int)), this, SLOT(checkAdminState(int)));
    grid->addWidget(asAdministrator, 2, 0);

    frame->setLayout(grid);
}

void LoginFrame::logInButtonListener() {
    if(strcmp(this->email->text().toLatin1().data(), "") == 0 && strcmp(this->password->text().toLatin1().data(), "") == 0 )
        QMessageBox::warning(NULL, "Message", "You didn't insert email/password!");
    else if(strcmp(this->email->text().toLatin1().data(), "") == 0)
        QMessageBox::warning(NULL, "Message", "You didn't insert email!");
    else if(strcmp(this->password->text().toLatin1().data(), "") == 0 )
        QMessageBox::warning(NULL, "Message", "You didn't insert password!");
    else {
        if (checked) logInAsAdministrator();
        else logInAsUser();

    }
}

void LoginFrame::logInAsAdministrator() {
    bool found = Options::logInAsAdmin(email->text().toLatin1().data(),
                                password->text().toLatin1().data(),
                                this->getSocketDescriptor());

    if (found) {
        setVisibility(false);

        logOutFromAdminMenu = new QPushButton("Log Out", this);
        connect(logOutFromAdminMenu, SIGNAL(clicked()), this, SLOT(logOutFromAdminListener()));

        adminMainMenu = new AdminMainMenu(this, socketDescriptor);

        grid->addWidget(adminMainMenu->getFrame(), 0, 0);
        grid->addWidget(logOutFromAdminMenu, 1, 0);

        QMessageBox::information(NULL, "Message", "You have been successfully logged in as Administrator!");
    }
    else QMessageBox::warning(NULL, "Message", "Invalid email/password for admin. Please try again.");
}

void LoginFrame::logInAsUser() {
    bool found = Options::logIn(email->text().toLatin1().data(),
                                password->text().toLatin1().data(),
                                this->getSocketDescriptor());

    if (found) {
        setVisibility(false);

        mainMenu = new MainMenu();
        mainMenu->setSocketDescriptor(socketDescriptor);
        mainMenu->setWelcomeMessage(email->text().toLatin1().data());

        logOut = new QPushButton("Log Out", this);
        connect(logOut, SIGNAL(clicked()), this, SLOT(logOutButtonListener()));
        grid->addWidget(mainMenu->getFrame(), 0, 0);
        grid->addWidget(logOut, 1, 0);

        mainMenu->logOut = logOut;
        QMessageBox::information(NULL, "Message", "You have been successfully logged in!");
    }
    else QMessageBox::warning(NULL, "Message", "Invalid email/password. Please try again.");
}

void LoginFrame::setSocketDescriptor(int* fd) {
    this->socketDescriptor = fd;
}

int LoginFrame::getSocketDescriptor() {
    return *this->socketDescriptor;
}

QFrame *LoginFrame::getFrame() {
    return this->frame;
}

void LoginFrame::setVisibility(bool visible) {
    backFromLogIn->setVisible(visible);
    logInButton->setVisible(visible);
    emailLabel->setVisible(visible);
    email->setVisible(visible);
    passwordLabel->setVisible(visible);
    password->setVisible(visible);
    asAdministrator->setVisible(visible);
}

void LoginFrame::logOutButtonListener() {
    logOut->setVisible(false);
    mainMenu->setVisibility(false);
    guestMenu->addGuestMenu();
    Options::logOut(*socketDescriptor);
}

void LoginFrame::checkAdminState(int state) {
    checked = state == Qt::Checked;
}

void LoginFrame::logOutFromAdminListener() {
    logOutFromAdminMenu->setVisible(false);
    adminMainMenu->setVisibility(false);
    guestMenu->addGuestMenu();
}
