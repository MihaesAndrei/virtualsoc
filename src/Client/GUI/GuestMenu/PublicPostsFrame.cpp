#include "PublicPostsFrame.h"

PublicPostsFrame::PublicPostsFrame(QWidget *parent) {

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame *PublicPostsFrame::getFrame() {

    char* text = Options::viewPublicPosts(*socketDescriptor);
    publicPosts = new QLabel(text, frame);
    QFont font( "Arial", 12);
    publicPosts->setFont(font);


    QLabel *title = new QLabel("Public posts:", frame);
    title->setTextFormat(Qt::TextFormat::RichText);


    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Light);
    scrollArea->setWidget(publicPosts);

    grid->addWidget(title, 0, 0);
    grid->addWidget(scrollArea, 1, 0);

    return this->frame;
}

void PublicPostsFrame::setSocketDescriptor(int *fd) {
    this->socketDescriptor = fd;
}

int PublicPostsFrame::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void PublicPostsFrame::setVisibility(bool visibile) {
    frame->setVisible(visibile);
}
