#ifndef VIRTUALSOC_LOGINFRAME_H
#define VIRTUALSOC_LOGINFRAME_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <src/Client/GUI/MainMenu/AdminMainMenu.h>

#include "../MainMenu/MainMenu.h"
#include "GuestMenu.h"
#include "src/Client/Options.h"

class MainMenu;
class GuestMenu;

class LoginFrame : public QWidget{

Q_OBJECT

public:
    LoginFrame(QWidget *parent = 0);
    QFrame* getFrame();
    void setVisibility(bool visible);

    void setSocketDescriptor(int* fd);
    int getSocketDescriptor();

    MainMenu *mainMenu;
    GuestMenu  *guestMenu;
    AdminMainMenu *adminMainMenu;
    QPushButton *backFromLogIn;
    QPushButton *logOutFromAdminMenu;
    QPushButton *logOut;

private slots:
    void logInButtonListener();
    void logOutButtonListener();
    void logOutFromAdminListener();
    void checkAdminState(int state);

private:
    bool checked = false;

private:
    QFrame *frame;
    QGridLayout *grid;

    int* socketDescriptor;

    QLabel *emailLabel;
    QLabel *passwordLabel;
    QLineEdit *email;
    QLineEdit *password;

    QPushButton *logInButton;

    QCheckBox *asAdministrator;

    void logInAsUser();

    void logInAsAdministrator();
};

#endif //VIRTUALSOC_LOGINFRAME_H
