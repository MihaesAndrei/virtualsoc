#ifndef VIRTUALSOC_GUESTMENU_H
#define VIRTUALSOC_GUESTMENU_H

#include <unistd.h>

#include <QWidget>
#include <QFrame>
#include <QPushButton>
#include <QGridLayout>
#include <QCoreApplication>

#include "LoginFrame.h"
#include "SignUpFrame.h"
#include "PublicPostsFrame.h"

class LoginFrame;
class SignUpFrame;
class PublicPostsFrame;

class GuestMenu : public QWidget {

Q_OBJECT

public:
    GuestMenu(QWidget *parent = 0);
    QFrame* getFrame();
    void setVisibility(bool visible);

    void addGuestMenu();
    void removeGuestMenu();

    void setSocketDescriptor(int* fd);
    int getSocketDescriptor();

    void closeSocket();

private slots:
    void logInListener();
    void quitListener();
    void signUpListener();
    void viewRecentPostsListener();

    void backFromLogInListener();
    void backFromSignUpListener();
    void backFromViewRecentPosts();


public:
    LoginFrame *loginFrame;
    SignUpFrame *signUpMenu;
    PublicPostsFrame *publicPostsFrame;

private:
    QFrame *frame;
    QGridLayout *grid;

    int* socketDescriptor;

    QPushButton *signUp;
    QPushButton *logIn;
    QPushButton *viewPublicPosts;
    QPushButton *quit;

    QPushButton *backFromLogIn;
    QPushButton *backFromSignUp;
    QPushButton *backFromRecentPosts;
};

#endif //VIRTUALSOC_MAINMENU_H
