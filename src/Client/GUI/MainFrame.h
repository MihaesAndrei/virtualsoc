#ifndef VIRTUALSOC_MAINFRAME_H
#define VIRTUALSOC_MAINFRAME_H

#include <QWidget>
#include <QBoxLayout>
#include <QMessageBox>
#include <QApplication>
#include <QApplication>
#include <QDesktopWidget>

#include "ConnectMenu.h"
#include "GuestMenu/GuestMenu.h"

class MainFrame : public QWidget{

Q_OBJECT

public:
    MainFrame(QWidget *parent = 0);
    void draw();

    int getSocketDescriptor();
    void setSocketDescriptor(int *fd);

private slots:
    void connectListener();
    void quitMyApp();

private:
    QGridLayout *grid;
    QFrame *frame;

    int* socketDescriptor;

    bool connected = false;

    GuestMenu *guestMenu;
    ConnectMenu *connectFrame;


    QPushButton *btn;
};

#endif //VIRTUALSOC_MAINFRAME_H
