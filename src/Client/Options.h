#ifndef VIRTUALSOC_OPTIONS_H
#define VIRTUALSOC_OPTIONS_H

#include <unistd.h>
#include <vector>
#include <iostream>
#include "src/Server/Utility.h"

using namespace std;

class PrivacySettings {
public:
    char* profilePrivacy = (char *) "";
    char* postsPrivacy = (char *) "";
};


class MyFriend {
public:
    MyFriend(char * info) {
        bzero(email, 100);
        bzero(type, 100);
        bzero(status, 100);
        char *pch = strtok (info," ,-");
        int i = 0;
        while (pch != NULL)
        {
            switch(i) {
                case 0: strcpy(email, pch); break;
                case 1: strcpy(type, pch); break;
                case 2: strcpy(status, pch); break;
                default: break;
            }
            pch = strtok (NULL, " ,-");
            i++;
        }
    }
    char email[100];
    char type[100];
    char status[100];
};

class UserInfo {
public:
    UserInfo(char * info) {
        bzero(firstname, 100);
        bzero(surname, 100);
        bzero(email, 100);
        bzero(status, 100);
        bzero(ip, 100);
        char *pch = strtok (info," -");
        int i = 0;
        while (pch != NULL)
        {
            switch(i) {
                case 0: strcpy(firstname, pch); break;
                case 1: strcpy(surname, pch); break;
                case 2: strcpy(email, pch); break;
                case 3: strcpy(status, pch); break;
                case 4: strcpy(ip, pch); break;
                default: break;
            }
            pch = strtok (NULL, " ,-");
            i++;
        }
    }
    char firstname[100];
    char surname[100];
    char email[100];
    char status[100];
    char ip[100];
};
class Options {

public:
    static bool logIn(char *email, char *password, int socketDescriptor);
    static bool signUp(char *firstName, char *surname, char *email, char *password, int socketDescriptor);
    static char *viewPublicPosts(int sockectDescriptor);
    static int addFriend(char *friendEmail, char *type, int socketDescriptor);
    static bool logOut(int socketDescriptor);
    static std::vector<MyFriend> viewFriendList(int socketDescriptor);
    static PrivacySettings*getPrivacySettings(int socketDescriptor);
    static void setProfilePrivacy(char* privacy, int socketDescriptor);
    static void setPostsPrivacy(char* privacy, int socketDescriptor);
    static void makePost(char* text, char* privacy, int socketDescriptor);
    static char *viewRecentPosts(int socketDescriptor);
    static bool createConversation(char* conversationName, int socketDescriptor);
    static bool addParticipant(char * conversationName, char *participantEmail, int socketDescriptor);
    static bool addOwnerInParticipants(char *conversationName, int socketDescriptor);
    static void sendMessage(char* conversationName, char* message, int socketDescriptor);
    static char* getMessages(char * conversationName, int socketDescriptor);
    static vector<string> getParticipants(char *conversationName, int socketDescriptor);
    static bool signUpAsAdmin(char *firstName, char *surname, char *email, char *password, int socketDescriptor);
    static bool logInAsAdmin(char *email, char *password, int socketDescriptor);
    static vector<UserInfo> getUsersList(int socketDescriptor);
    static vector<string> getConversationsList(int socketDescriptor);


};


#endif //VIRTUALSOC_OPTIONS_H
