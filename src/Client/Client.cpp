#include "Client.h"

Client::Client(char *ip, char *port) {
    *socketDescriptor = createSocket(AF_INET, SOCK_STREAM, 0);
    initializeServer(ip, port);
    connectToServer();
}

int Client::createSocket(int domain, int type, int protocol) {
    /* cream socketul */
    int socketDescriptor;
    if ((socketDescriptor = socket(domain, type, protocol)) == -1) throw "[client] Error: socket().\n";
    return socketDescriptor;
}

void Client::initializeServer(char *ip, char *port) {

    this->port = (uint16_t) atoi(port);
    /* umplem structura folosita pentru realizarea conexiunii cu serverul */

    /* familia socket-ului */
    server.sin_family = AF_INET; /* adresa IP a serverului */
    server.sin_addr.s_addr = inet_addr(ip); /* portul de conectare */
    server.sin_port = htons(this->port);
}

void Client::connectToServer() {
    /* ne conectam la server */
    if (connect(*socketDescriptor, (struct sockaddr *) &server, sizeof(struct sockaddr)) == -1)
        throw "[client]Error: connect().\n";
}

int* Client::getSocketDescriptor() {
    return this->socketDescriptor;
}