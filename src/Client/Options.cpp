#include <stdio.h>
#include "Options.h"

bool Options::logIn(char *email, char *password, int socketDescriptor) {
    if (write(socketDescriptor, "logIn", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, email, 100) < 0) throw "[client]Error: write() to server.\n";
    bool received;
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, password, 100) < 0) throw "[client]Error: write() to server.\n";

    bool found;
    if (read(socketDescriptor, &found, 100) < 0) throw "[client]Error: read() from server.\n";
    return found;
}

bool Options::signUp(char *firstName, char *surname, char *email, char *password, int socketDescriptor) {
    if (write (socketDescriptor, "signUp", sizeof("signUp")) < 0) throw "[client]Error: write() to server.\n";

    bool received;
    /* Firstname */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, firstName, 100) < 0) throw "[client]Error: write() to server.\n";

    /* Surname */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, surname, 100) < 0) throw "[client]Error: write() to server.\n";

    /* Password */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, password, 100) < 0) throw "[client]Error: write() to server.\n";

    /* Email */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, email, 100) < 0) throw "[client]Error: write() to server.\n";

    bool inserted;
    if (read (socketDescriptor, &inserted, 100) < 0) throw "[client]Error: read() from server.\n";

    printf(inserted ? "You have been successfully registered!\n"
                    : "The e-mail address you specified is already in use.\n");
    return inserted;
}

char *Options::viewPublicPosts(int socketDescriptor) {
    if (write(socketDescriptor, "viewPublicPosts", 100) < 0) throw "[client]Error: write() to server.\n";

    char *publicPosts = (char *) "";
    int posts = 0;
    while (posts >= 0) {
        bool readyToRead = true;
        char post[100]; bzero(post, 100);

        if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
        if (read(socketDescriptor, &post, sizeof(post)) < 0)throw "[client]Eroare la read() de la server.\n";

        if (strcmp(post, "break") == 0) break;
        else {
            posts++;
            publicPosts = Utility::concatenate(publicPosts, post);
            publicPosts = Utility::concatenate(publicPosts, (char *) "\n");
        }
    }
    if(posts == 0) publicPosts = Utility::concatenate(publicPosts, (char *) "There are no posts yet.\n");
    return publicPosts;
}

int Options::addFriend(char *friendEmail, char *type, int socketDescriptor) {

    if (write(socketDescriptor, "addFriend", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, friendEmail, 100) < 0) throw "[client]Error: write() to server.\n";
    if (write(socketDescriptor, type, 100) < 0) throw "[client]Error: write() to server.\n";

    bool inserted;
    if (read(socketDescriptor, &inserted, sizeof(inserted)) < 0) throw "[client]Eroare la read() de la server.\n";

    if (inserted) { printf("\"%s\" has been added successfully to your friend list.\n", friendEmail); return 1; }
    else { printf("\"%s\" is already in your friend list.\n", friendEmail); return 0; }
}

bool Options::logOut(int socketDescriptor) {
    if (write(socketDescriptor, "logOut", 100) < 0) throw "[client]Error: write() to server.\n";
    printf("You have been successfully logged out!\n");
    return true;
}

std::vector<MyFriend> Options::viewFriendList(int socketDescriptor) {
    if (write(socketDescriptor, "viewFriendList", 100) < 0) throw "[client]Error: write() to server.\n";

    char* friendList = (char *) "";

    vector<MyFriend> myFriends;
    int friends = 0;
    while(friends >= 0) {
        bool readyToRead =  true;
        char info[50]; bzero(info, 50);

        if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
        if (read(socketDescriptor, &info, sizeof(info)) < 0)throw "[client]Eroare la read() de la server.\n";

        if(strcmp(info, "break") == 0) break;
        else {
            friendList = Utility::concatenate(friendList, info);

            MyFriend myFriend(info);
            myFriends.push_back(myFriend);

            friendList = Utility::concatenate(friendList, (char *) "\n");
            friends++;
        }
    }
    return myFriends;
}

PrivacySettings* Options::getPrivacySettings(int socketDescriptor) {
    if (write(socketDescriptor, "privacySettings", 100) < 0) throw "[client]Error: write() to server.\n";

    char profilePrivacy[100]; bzero(profilePrivacy, 100);
    if (read(socketDescriptor, &profilePrivacy, sizeof(profilePrivacy)) < 0) throw "[client]Eroare la read() de la server.\n";

    bool readyToRead = true;
    if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";

    char postsPrivacy[100]; bzero(postsPrivacy, 100);
    if (read(socketDescriptor, &postsPrivacy, sizeof(postsPrivacy)) < 0) throw "[client]Eroare la read() de la server.\n";

    PrivacySettings *settings = new PrivacySettings();
    settings->profilePrivacy = Utility::concatenate(settings->profilePrivacy, profilePrivacy);
    settings->postsPrivacy = Utility::concatenate(settings->postsPrivacy, postsPrivacy);
    return settings;
}

void Options::setProfilePrivacy(char *privacy, int socketDescriptor) {
    if (write(socketDescriptor, "setProfilePrivacy", 100) < 0) throw "[client]Error: write() to server.\n";
    if (write(socketDescriptor, privacy, sizeof(privacy)) < 0) throw "[client]Error: write() to server.\n";
}

void Options::setPostsPrivacy(char *privacy, int socketDescriptor) {
    if (write(socketDescriptor, "setPostsPrivacy", 100) < 0) throw "[client]Error: write() to server.\n";
    if (write(socketDescriptor, privacy, sizeof(privacy)) < 0) throw "[client]Error: write() to server.\n";
}

void Options::makePost(char *post, char *privacy, int socketDescriptor) {
    if (write(socketDescriptor, "makePost", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, post, strlen(post)) < 0) throw "[client]Error: write() to server.\n";

    bool received;
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";

    if (write(socketDescriptor, privacy, sizeof(privacy)) < 0) throw "[client]Error: write() to server.\n";
    else printf("Post submited!\n");
}

char *Options::viewRecentPosts(int socketDescriptor) {
    if (write(socketDescriptor, "viewRecentPosts", 100) < 0) throw "[client]Error: write() to server.\n";

    char *recentPosts = (char *) "";

    int posts = 0;
    while(posts >= 0) {
        bool readyToRead =  true;
        char post[100]; bzero(post, 100);

        if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
        if (read(socketDescriptor, &post, sizeof(post)) < 0)throw "[client]Eroare la read() de la server.\n";

        if(strcmp(post, "break") == 0) break;
        else {
            posts++;
            recentPosts = Utility::concatenate(recentPosts, post);
            recentPosts = Utility::concatenate(recentPosts, (char *) "\n");
        }
    }
    if(posts == 0) recentPosts = Utility::concatenate(recentPosts, (char *) "There are no posts yet.\n");
    return recentPosts;
}

bool Options::createConversation(char *conversationName, int socketDescriptor) {
    if (write(socketDescriptor, "createConversation", 100) < 0) throw "[client]Error: write() to server.\n";

    bool received;
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";

    if (write(socketDescriptor, conversationName, sizeof(conversationName)) < 0) throw "[client]Error: write() to server.\n";

    bool inserted;
    if (read(socketDescriptor, &inserted, sizeof(inserted)) < 0)throw "[client]Eroare la read() de la server.\n";

    return inserted;
}

bool Options::addParticipant(char *conversationName, char *participantEmail, int socketDescriptor) {
    if (write(socketDescriptor, "addParticipant", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, conversationName, 100) < 0) throw "[client]Error: write() to server.\n";
    if (write(socketDescriptor, participantEmail, 100) < 0) throw "[client]Error: write() to server.\n";

    bool inserted;
    if (read(socketDescriptor, &inserted, sizeof(inserted)) < 0)throw "[client]Eroare la read() de la server.\n";

    return inserted;
}

bool Options::addOwnerInParticipants(char *conversationName, int socketDescriptor) {
    if (write(socketDescriptor, "addOwnerInParticipants", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, conversationName, 100) < 0) throw "[client]Error: write() to server.\n";
    bool inserted;
    if (read(socketDescriptor, &inserted, sizeof(inserted)) < 0)throw "[client]Eroare la read() de la server.\n";

    return inserted;
}

void Options::sendMessage(char *conversationName, char *message, int socketDescriptor) {
    if (write(socketDescriptor, "sendMessage", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, conversationName, 100) < 0) throw "[client]Error: write() to server.\n";
    if (write(socketDescriptor, message, 100) < 0) throw "[client]Error: write() to server.\n";
}

char* Options::getMessages(char *conversationName, int socketDescriptor) {
    if (write(socketDescriptor, "getMessages", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, conversationName, 100) < 0) throw "[client]Error: write() to server.\n";

    bool readyToRead = true;
    if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";

    char messages[2000]; bzero(messages, 2000);
    if (read(socketDescriptor, messages, sizeof(messages)) < 0)throw "[client]Eroare la read() de la server.\n";

    char *result = (char *) malloc(sizeof(messages));

    strcpy(result, messages);
    return result;
}

vector<string> Options::getParticipants(char *conversationName, int socketDescriptor) {
    if (write(socketDescriptor, "getParticipants", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, conversationName, 100) < 0) throw "[client]Error: write() to server.\n";


    vector<string> p;

    int participants = 0;
    while(participants >= 0) {
        bool readyToRead =  true;
        char participantEmail[100]; bzero(participantEmail, 100);

        if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
        if (read(socketDescriptor, &participantEmail, sizeof(participantEmail)) < 0)throw "[client]Eroare la read() de la server.\n";

        if(strcmp(participantEmail, "break") == 0) break;
        else {
            participants++;
            p.push_back(participantEmail);
        }
    }
    return p;
}

bool Options::signUpAsAdmin(char *firstName, char *surname, char *email, char *password, int socketDescriptor) {
    if (write (socketDescriptor, "signUpAsAdmin", 100) < 0) throw "[client]Error: write() to server.\n";

    bool received;
    /* Firstname */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, firstName, 100) < 0) throw "[client]Error: write() to server.\n";

    /* Surname */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, surname, 100) < 0) throw "[client]Error: write() to server.\n";

    /* Password */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, password, 100) < 0) throw "[client]Error: write() to server.\n";

    /* Email */
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, email, 100) < 0) throw "[client]Error: write() to server.\n";

    bool inserted;
    if (read (socketDescriptor, &inserted, 100) < 0) throw "[client]Error: read() from server.\n";
    return inserted;
}

bool Options::logInAsAdmin(char *email, char *password, int socketDescriptor) {
    if (write(socketDescriptor, "logInAsAdmin", 100) < 0) throw "[client]Error: write() to server.\n";

    if (write(socketDescriptor, email, 100) < 0) throw "[client]Error: write() to server.\n";
    bool received;
    if (read(socketDescriptor, &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(socketDescriptor, password, 100) < 0) throw "[client]Error: write() to server.\n";

    bool found;
    if (read(socketDescriptor, &found, 100) < 0) throw "[client]Error: read() from server.\n";
    return found;
}

vector<UserInfo> Options::getUsersList(int socketDescriptor) {

    if (write(socketDescriptor, "getUsersList", 100) < 0) throw "[client]Error: write() to server.\n";

    vector<UserInfo> usersInfo;
    int users = 0;
    while(users >= 0) {
        bool readyToRead =  true;
        char info[100]; bzero(info, 100);

        if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
        if (read(socketDescriptor, &info, sizeof(info)) < 0)throw "[client]Eroare la read() de la server.\n";

        if(strcmp(info, "break") == 0) break;
        else {
            UserInfo userInfo(info);
            usersInfo.push_back(userInfo);
            users++;
        }
    }

    return usersInfo;
}

vector<string> Options::getConversationsList(int socketDescriptor) {
    if (write(socketDescriptor, "getConversationsList", 100) < 0) throw "[client]Error: write() to server.\n";


    vector<string> conversationsList;

    int conversations = 0;
    while(conversations >= 0) {
        bool readyToRead =  true;
        char conversationName[100]; bzero(conversationName, 100);

        if (write(socketDescriptor, &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
        if (read(socketDescriptor, &conversationName, sizeof(conversationName)) < 0)throw "[client]Eroare la read() de la server.\n";

        if(strcmp(conversationName, "break") == 0) break;
        else {
            conversations++;
            conversationsList.push_back(conversationName);
        }
    }
    return conversationsList;
}
