#include "Client/GUI/MainFrame.h"

int main(int argc, char *argv[]) {

    try {
        QApplication app(argc, argv);

        MainFrame window;

        window.draw();
        window.setWindowTitle("VirtualSoc");

        return app.exec();
    }
    catch(const char *string) { perror(string); }
}