#ifndef VIRTUALSOC_SERVER_H
#define VIRTUALSOC_SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>

#include "MenuOption.h"
#include "Database/User.h"

class Server {

private:
    uint16_t port;
    struct sockaddr_in server;
    int serverDescriptor;

public:
    Server(int port);
    void start();

private:
    int createSocket(int domain, int type, int protocol);
    void bindSocket(int &serverDescriptor, sockaddr_in &server);
    void acceptClients(int &serverDescriptor);

};
#endif //VIRTUALSOC_SERVER_H
