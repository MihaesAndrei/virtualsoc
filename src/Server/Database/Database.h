#ifndef VIRTUALSOC_DATABASE_H
#define VIRTUALSOC_DATABASE_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sqlite3.h>
#include "User.h"

#include <string>
#include <vector>
using namespace std;

class Database {

private:
    sqlite3 *db;
    char *zErrMsg;
    int rc;
    char *sql;
    const char *data;

public:
    static bool count;
    static User user;
    static char messages[];
    static vector<string> participants;
    static int validMail;
    static int adminFound;

public:
    Database();
    ~Database();

    bool insertUser(User &user);
    bool findUser(User &user);
    void setStatusOnline(User &user);
    void setStatusOffline(User &user);
    bool insertFriendship(User &user1, User& user2, char* type);
    void getFriendList(User &user);
    bool getUserPrivacy(User &user);
    void setProfilePrivacy(User &user, char* value);
    void setPostsPrivacy(User &user, char* value);
    void insertPost(User &user, char* post, char* privacy);
    void selectPublicPosts(User &user);
    void selectRecentPosts(User &user);
    bool insertConversation(User &user, char *conversationName);
    bool insertParticipant(User &user, char* conversationName, char *participantEmail);
    bool insertOwnerInParticipants(User &user, char *conversationName);
    void insertMessage(User& user, char* conversationName, char* message);
    char* getMessages(User& user, char* conversationName);
    vector<string> getParticipants(User& user, char *conversationName);
    bool insertAdministrator(User& user);
    int validEmail(User &user, char *email);
    int findAdministrator(User &user);
    void getUsersList(User& user);
    void getConversationsList(User& user);


private:
    static int callback_insert(void *NotUsed, int argc, char **argv, char **azColName);
    static int callback_find(void *data, int argc, char **argv, char **azColName);
    static int callback_updateStatus(void *data, int argc, char **argv, char **azColName);
    static int callback_getFriendList(void *data, int argc, char **argv, char **azColName);
    static int callback_getUserPrivacy(void *data, int argc, char **argv, char **azColName);
    static int callback_setPrivacy(void *data, int argc, char **argv, char **azColName);
    static int callback_publicPosts(void *data, int argc, char **argv, char **azColName);
    static int callback_recentPosts(void *data, int argc, char **argv, char **azColName);
    static int callback_getMessages(void *data, int argc, char **argv, char **azColName);
    static int callback_getParticipants(void *data, int argc, char **argv, char **azColName);
    static int callback_validEmail(void *data, int argc, char **argv, char **azColName);
    static int callback_findAdministrator(void *data, int argc, char **argv, char **azColName);
    static int callback_getUsersList(void *data, int argc, char **argv, char **azColName);
    static int callback_getConversationsList(void *data, int argc, char **argv, char **azColName);


};

#endif //VIRTUALSOC_DATABASE_H
