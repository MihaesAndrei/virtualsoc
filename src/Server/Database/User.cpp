#include "User.h"

char *User::getFirstName() { return this->firstname; }
char *User::getSurname() { return this->surname; }
char *User::getEmail() { return this->email; }
char *User::getPassword() { return this->password; }
int User::getFileDescriptor() { return this->fileDescriptor; }
char *User::getAddress() { return this->address; }

void User::setFirstName(char *firstname) { Utility::copy(&this->firstname, firstname); }
void User::setSurname(char *surname) { Utility::copy(&this->surname, surname); }
void User::setEmail(char *email) { Utility::copy(&this->email, email); }
void User::setPassword(char *password) { Utility::copy(&this->password, password); }
void User::setFileDescriptor(int fileDescriptor) { this->fileDescriptor = fileDescriptor; }
void User::setAddress(sockaddr_in address) { Utility::copy(&this->address, Utility::conv_addr(address)); }
void User::setAddress(char *address) { Utility::copy(&this->address, address); }

User::User(User &user) {
    this->setFirstName(user.getFirstName());
    this->setSurname(user.getSurname());
    this->setEmail(user.getEmail());
    this->setPassword(user.getPassword());
    this->setFileDescriptor(user.getFileDescriptor());
    this->setAddress(user.getAddress());
}