#ifndef VIRTUALSOC_UTILITY_H
#define VIRTUALSOC_UTILITY_H

#include <stdlib.h> /* malloc() */
#include <string.h> /* strlen(), memcpy() */
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h> /* sprintf() */

class Utility {

public:
    static char* concatenate(char* first, char* second);
    static void copy(char** first, char* second);
    static char * conv_addr(sockaddr_in address);
};
#endif //VIRTUALSOC_UTILITY_H
